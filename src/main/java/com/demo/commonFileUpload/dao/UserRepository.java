package com.demo.commonFileUpload.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.commonFileUpload.model.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

	Users findByEmail(String email);
}
