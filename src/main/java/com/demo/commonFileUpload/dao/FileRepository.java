package com.demo.commonFileUpload.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.commonFileUpload.model.Files;

public interface FileRepository extends JpaRepository<Files, Long>{

	@Query("SELECT f from Files f where f.id = :id and f.ownerId = :ownerId")
	Optional<Files> findByIdandOwner(Long id, Long ownerId);
}
