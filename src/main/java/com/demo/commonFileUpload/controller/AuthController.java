package com.demo.commonFileUpload.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.commonFileUpload.dto.http.AbstractResponse;
import com.demo.commonFileUpload.dto.http.AuthRequest;
import com.demo.commonFileUpload.exceptions.GenericException;
import com.demo.commonFileUpload.service.AuthUserDetailService;

@RestController
public class AuthController {

	@Autowired
	AuthUserDetailService userDetailService;
	
	@PostMapping("/register")
	ResponseEntity<?> register(@RequestBody @Valid AuthRequest req) throws GenericException
	{
		AbstractResponse response = new AbstractResponse();
		
		Long userId = userDetailService.save(req);
		
		response.setSuccess(true);
		response.setMessage("User Successfully registered with Id : " + userId);
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
}
