package com.demo.commonFileUpload.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.demo.commonFileUpload.dto.http.FileSearchResponse;
import com.demo.commonFileUpload.exceptions.GenericException;
import com.demo.commonFileUpload.service.FilesService;
import com.google.common.net.HttpHeaders;

@RestController
@RequestMapping(value = "/api")
public class FileController {

	@Autowired
	FilesService fileService;
	
	@GetMapping("/file")
	public ResponseEntity<FileSearchResponse> getAllFiles() throws GenericException
	{
		return null;
	}
	
	@GetMapping("/file/{id}")
	public ResponseEntity<Resource> downloadFile(@PathVariable(name = "id")String id, 
			HttpServletRequest req) throws Exception
	{
		
        Resource resource = fileService.downloadFile(Long.valueOf(id));

        String contentType = null;
        try {
            contentType = req.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
          
        	throw new HttpMediaTypeNotSupportedException("Invalid Content-type");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
		
	}
	
	@PostMapping(value = "/file", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws GenericException, IOException{
		
		
		fileService.uploadFile(file);
		return null;
	}
	
	@PostMapping("/share")
	public ResponseEntity<?> shareFile() throws GenericException{
		
		return null;
	}
	
}
