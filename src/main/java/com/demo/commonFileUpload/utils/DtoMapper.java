package com.demo.commonFileUpload.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AuthRequest;
import com.demo.commonFileUpload.model.Users;

@Component
public class DtoMapper {
	
	@Autowired
	PasswordEncoder encoder;
	

	public void mapAuthToUser(AuthRequest request, Users user)
	{
		user.setEmail(request.getEmail());
		user.setPassword(encoder.encode(request.getPassword()));
	}
}
