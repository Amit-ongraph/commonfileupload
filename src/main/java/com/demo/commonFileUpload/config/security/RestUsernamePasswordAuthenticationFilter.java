package com.demo.commonFileUpload.config.security;

import java.io.BufferedReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AuthRequest;
import com.demo.commonFileUpload.utils.JsonUtis;
import com.fasterxml.jackson.core.type.TypeReference;


public class RestUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	static final String ORIGIN = "Origin";

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {

		AuthRequest loginRequest = null;

		try {
			loginRequest = parseLoginData(request);
			// request.getSession().setAttribute("portal", loginRequest.getPortal());

		} catch (Exception e) {
			throw new AuthenticationServiceException(e.getLocalizedMessage());
		}

		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
				loginRequest.getEmail(), loginRequest.getPassword());

		setDetails(request, authRequest);

		return this.getAuthenticationManager().authenticate(authRequest);
	}

	private AuthRequest parseLoginData(HttpServletRequest request) throws Exception {
		StringBuffer requestBodyString = new StringBuffer();

		String line = null;

		BufferedReader reader = request.getReader();

		while ((line = reader.readLine()) != null) {
			requestBodyString.append(line);
		}

		return JsonUtis.getObjectFromJson(requestBodyString.toString(), new TypeReference<AuthRequest>() {
		});

	}


}
