package com.demo.commonFileUpload.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.demo.commonFileUpload.service.AuthUserDetailService;

@Configuration
public class GlobalSecurityConfiguration extends WebSecurityConfigurerAdapter{

	@Autowired
	RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;

	@Autowired
	RestAuthenticaitonFailureHandler restAuthenticationFailureHandler;

	@Autowired
	RestAuthenticationEntyPoint restAuthenticationEntryPoint;

	@Autowired
	AuthUserDetailService authUserDetailService;

	@Autowired
	RestLogoutSuccesHandler restLogoutSuccessHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

		/*
		 * ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
		 * authenticationManagerBuilder.userDetailsService(userDetailsService).
		 * passwordEncoder(encoder);
		 */
		PasswordEncoder encoder = new BCryptPasswordEncoder(4);
		authenticationManagerBuilder.userDetailsService(authUserDetailService).passwordEncoder(encoder);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.debug(true);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();
		http.exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);
		http.authorizeRequests().antMatchers(HttpMethod.OPTIONS).permitAll();
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		http.authorizeRequests().antMatchers("/register").permitAll();
		http.formLogin().loginPage("/login").permitAll();
		 http.authorizeRequests()
         .antMatchers("/v2/api-docs", "/configuration/**", "/swagger-resources", "/configuration/security",
                 "/swagger-ui.html", "/webjars/**", "/swagger-resources/configuration/**", "/swagger-ui.html",
                 "/swagger-resources/configuration/security").permitAll()
         .antMatchers("/actuator/**").permitAll()
         .antMatchers("/**").authenticated();
		http.logout().logoutUrl("/logout").logoutSuccessHandler(restLogoutSuccessHandler);

		http.authorizeRequests().anyRequest().authenticated();

		http.addFilter(jwtAuthorizationFilter());
		http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	public RestUsernamePasswordAuthenticationFilter authenticationFilter() throws Exception {

		RestUsernamePasswordAuthenticationFilter authFilter = new RestUsernamePasswordAuthenticationFilter();

		authFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
		authFilter.setAuthenticationManager(authenticationManagerBean());
		authFilter.setAuthenticationSuccessHandler(restAuthenticationSuccessHandler);
		authFilter.setAuthenticationFailureHandler(restAuthenticationFailureHandler);
		authFilter.setUsernameParameter("username");
		authFilter.setPasswordParameter("password");

		return authFilter;
	}

	@Bean
	public JwtAuthorizationFilter jwtAuthorizationFilter() throws Exception {

		JwtAuthorizationFilter jwtAuthorizationFilter = new JwtAuthorizationFilter(authenticationManagerBean());

		return jwtAuthorizationFilter;
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}

}
