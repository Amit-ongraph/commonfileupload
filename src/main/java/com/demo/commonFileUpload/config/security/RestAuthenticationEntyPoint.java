package com.demo.commonFileUpload.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AbstractResponse;
import com.demo.commonFileUpload.utils.JsonUtis;

@Component
public class RestAuthenticationEntyPoint implements AuthenticationEntryPoint{
	
	static final String ORIGIN = "Origin";

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			org.springframework.security.core.AuthenticationException authException)
			throws IOException, ServletException {
		if ("application/json".equals(request.getHeader("Content-Type"))) {

			AbstractResponse jsonResponse = new AbstractResponse();

			response.setHeader("Access-Control-Allow-Origin", request.getHeader(ORIGIN));
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
			response.setHeader("Access-Control-Max-Age", "3600");
			response.setHeader("Access-Control-Allow-Headers",
					"x-requested-with, Content-Type, origin, authorization, accept, client-security-token");

			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			response.getWriter().print(JsonUtis.writeJsonString(jsonResponse));
			response.getWriter().flush();
		} else {

			AbstractResponse jsonResponse = new AbstractResponse();
			jsonResponse.setSuccess(false);
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

			response.getWriter().print(JsonUtis.writeJsonString(jsonResponse));
			response.getWriter().flush();

		}
	}

}
