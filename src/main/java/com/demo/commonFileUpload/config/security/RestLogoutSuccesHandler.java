package com.demo.commonFileUpload.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AuthResponse;
import com.demo.commonFileUpload.utils.JsonUtis;
import com.demo.commonFileUpload.utils.JwtTokenUtils;

@Component
public class RestLogoutSuccesHandler extends SimpleUrlLogoutSuccessHandler{

	static final String ORIGIN = "Origin";

	@Autowired
	private JwtTokenUtils jwtTokenUtil;

	/*
	 * @Autowired private UserService userService;
	 */

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {

		if ("application/json".equals(request.getHeader("Content-Type")) || request.getHeader("Content-Type") == null) {

			if (request.getHeader(ORIGIN) != null) {

				String origin = request.getHeader(ORIGIN);

				response.setHeader("Access-Control-Allow-Origin", origin);
				response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
				response.setHeader("Access-Control-Allow-Credentials", "true");
				response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
				response.setHeader("Content-Type", request.getHeader("Content-Type"));
			}

			try {
				String token = request.getHeader("authorization");
				token = token.replaceAll("Bearer", "").trim();

				// userService.markUserLoggedOut(jwtTokenUtil.getUsernameFromToken(token));

				/*
				 * if (tokenList.contains(token)) { tokenList.remove(token); }
				 */
			} catch (Exception e) {
			}

			AuthResponse jsonResponse = new AuthResponse();
			jsonResponse.setLogout(true);
			jsonResponse.setSuccess(true);

			response.getWriter().print(JsonUtis.writeJsonString(jsonResponse));
			response.getWriter().flush();
		}

	}
}
