package com.demo.commonFileUpload.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AbstractResponse;
import com.demo.commonFileUpload.utils.JsonUtis;

@Component
public class RestAuthenticaitonFailureHandler extends SimpleUrlAuthenticationFailureHandler{

	private String ORIGIN = "Origin";

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws ServletException, IOException {

		AbstractResponse jsonResponse = new AbstractResponse();

		jsonResponse.setMessage(exception.getLocalizedMessage().toString());

		if (request.getHeader(ORIGIN) != null) {

			String origin = request.getHeader(ORIGIN);

			response.setHeader("Access-Control-Allow-Origin", origin);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
		}

		response.getWriter().print(JsonUtis.writeJsonString(jsonResponse));
		response.getWriter().flush();

	}
}
