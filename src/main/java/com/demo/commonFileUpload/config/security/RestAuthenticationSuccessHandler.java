package com.demo.commonFileUpload.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.demo.commonFileUpload.dto.http.AuthResponse;
import com.demo.commonFileUpload.service.CoreService;
import com.demo.commonFileUpload.utils.JsonUtis;
import com.demo.commonFileUpload.utils.JwtTokenUtils;

@Component
public class RestAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{
	
	private String ORIGIN = "Origin";

	@Autowired
	private CoreService coreService;

	@Autowired
	private JwtTokenUtils jwtTokenUtil;

	/*
	 * @Autowired private List<String> tokenList;
	 */

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {

		AuthResponse jsonResponse = new AuthResponse();

		jsonResponse.setSuccess(true);
		jsonResponse.setIsAuthenticated(true);
		jsonResponse.setToken(jwtTokenUtil.generateToken(coreService.getLoggedInUser()));

		/*
		 * if (tokenList == null) { tokenList = new ArrayList<String>(); }
		 */

		// tokenList.add(jsonResponse.getToken());

		if (request.getHeader(ORIGIN) != null) {

			String origin = request.getHeader(ORIGIN);

			response.setHeader("Access-Control-Allow-Origin", origin);
			response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
		}

		response.getWriter().print(JsonUtis.writeJsonString(jsonResponse));
		response.getWriter().flush();

	}


}
