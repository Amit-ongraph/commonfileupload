package com.demo.commonFileUpload.dto.http;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(value = Include.NON_EMPTY)
public class AbstractResponse {

	Boolean success = Boolean.FALSE;
	String message;
	Boolean isAuthenticated = Boolean.FALSE;
	Map<String, String> errorMap = new HashMap<>();
	
}
