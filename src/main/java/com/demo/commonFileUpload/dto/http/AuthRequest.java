package com.demo.commonFileUpload.dto.http;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAlias;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthRequest {

	@JsonAlias(value = {"username", "email"})
	@NotNull
	@Email
	String email;
	
	@NotNull
	String password;
	
}
