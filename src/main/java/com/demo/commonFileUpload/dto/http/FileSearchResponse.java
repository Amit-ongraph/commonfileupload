package com.demo.commonFileUpload.dto.http;

import java.util.List;

import com.demo.commonFileUpload.model.Files;

public class FileSearchResponse extends AbstractResponse {

	List<Files> shared;
	List<Files> owned;
}
