package com.demo.commonFileUpload.dto.http;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthResponse extends AbstractResponse{

	String token;
	Boolean logout;
}
