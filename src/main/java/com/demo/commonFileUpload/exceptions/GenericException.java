package com.demo.commonFileUpload.exceptions;

public class GenericException extends Exception {

	public GenericException(String string) {
		
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -4779184831447999738L;

}
