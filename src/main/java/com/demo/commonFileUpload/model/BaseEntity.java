package com.demo.commonFileUpload.model;


import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class BaseEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	
	@CreatedDate
	Date createdAt;
	
	@LastModifiedDate
	Date updatedAt;
}