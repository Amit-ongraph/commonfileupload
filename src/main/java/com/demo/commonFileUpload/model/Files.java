package com.demo.commonFileUpload.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;

@Entity
@DynamicUpdate
@Getter
@Setter
public class Files extends BaseEntity{

	String name;
	String url;
	
	Long ownerId;
	
	@ManyToOne
	@JoinColumn(name="ownerId",insertable = false, updatable = false)
	Users owner;
	
	
}
