package com.demo.commonFileUpload.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "user")
@Getter
@Setter
public class Users extends BaseEntity{

	String email;
	String password;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "ownerId")
	List<Files> owned;
	
	@ManyToMany
	List<Files> collaborated;
	
	
}
