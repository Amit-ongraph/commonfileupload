package com.demo.commonFileUpload.service;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

import javax.activity.InvalidActivityException;
import javax.transaction.Transactional;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.demo.commonFileUpload.dao.FileRepository;
import com.demo.commonFileUpload.exceptions.GenericException;
import com.demo.commonFileUpload.model.Files;




@Service
public class FilesService {

	 private final Path fileStorageLocation;

	 
	
	final String uploadDirectory;
	
	@Autowired
	FileRepository fileRepository;
	
	@Autowired
	CoreService service;
	
	public FilesService(@Value("${com.demo.uploads.directory}") String dirRoot) {
	
		this.uploadDirectory = dirRoot;
		this.fileStorageLocation = Paths.get(uploadDirectory).toAbsolutePath().normalize();
	}
	
	@Transactional
	public Long uploadFile(MultipartFile file) throws GenericException, IOException
	{
		String uploadedFileName = StringUtils.cleanPath(file.getOriginalFilename());
		Long fileId = 0L;
		try {
			if(uploadedFileName.contains("..")) {
                throw new GenericException("Sorry! Filename contains invalid path sequence " + uploadedFileName);
            }
			Path targetLocation = this.fileStorageLocation.resolve(uploadedFileName);
			
			String basefileName = FilenameUtils.getBaseName(uploadedFileName);
			String ext = FilenameUtils.getExtension(uploadedFileName);
			
			
			if(targetLocation.toFile().exists()){
				
				uploadedFileName = basefileName.concat("_1").concat(FilenameUtils.EXTENSION_SEPARATOR_STR).concat(ext);
				targetLocation = this.fileStorageLocation.resolve(uploadedFileName);
			}
            java.nio.file.Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            
            Files files = new Files();
   		 
   		 files.setName(uploadedFileName);
   		 files.setUrl(targetLocation.toAbsolutePath().toUri().toString());
   		 files.setOwnerId(service.getLoggedInUserId());
   		 
   		 fileRepository.save(files);
   		 fileId =  files.getId();
   		 
		}catch(IOException e) {
			throw new GenericException("Unable To upload file to the destination Folder");
			
		}
		return fileId;
	}
	
	public Resource downloadFile(Long id) throws Exception{
		
		Optional<Files> file = fileRepository.findByIdandOwner(id, service.getLoggedInUserId());
		
		if(file.isPresent())
		{
			try {
				String uploadedFileName = file.get().getName();
	            Path filePath = this.fileStorageLocation.resolve(uploadedFileName).normalize();
	            Resource resource = new UrlResource(filePath.toUri());
	            if(resource.exists()) {
	                return resource;
	            } else {
	                throw new GenericException("File not found " + uploadedFileName);
	            }
	        } catch (MalformedURLException ex) {
	            throw new GenericException("File not found ");
	        }
			
		}else
			throw new GenericException("No Record with the Given Id");	
	}
	
}
