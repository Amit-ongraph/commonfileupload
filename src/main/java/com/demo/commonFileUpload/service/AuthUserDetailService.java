package com.demo.commonFileUpload.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.stereotype.Service;

import com.demo.commonFileUpload.dao.UserRepository;
import com.demo.commonFileUpload.dto.http.AuthRequest;
import com.demo.commonFileUpload.exceptions.GenericException;
import com.demo.commonFileUpload.model.Users;
import com.demo.commonFileUpload.utils.DtoMapper;



@Service
public class AuthUserDetailService implements UserDetailsService{

	@Autowired
	DtoMapper _mapper;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Users appUser = userRepository.findByEmail(username);
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		if(appUser == null)
			throw new UsernameNotFoundException("No User Exisit with this email");
		
		User user = new User(appUser.getEmail(), appUser.getPassword(), authorities);
		return user;
	}
	
	public Long save(AuthRequest request) throws GenericException
	{
		Users newUser = new Users();
		_mapper.mapAuthToUser(request, newUser);
		userRepository.save(newUser);
		
		return newUser.getId();
	}

}
