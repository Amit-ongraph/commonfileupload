package com.demo.commonFileUpload.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.demo.commonFileUpload.dao.UserRepository;
import com.demo.commonFileUpload.exceptions.GenericException;

@Service
public class CoreService {

	@Autowired
	UserRepository userRepository;
	
	public UserDetails getLoggedInUser()
	{
		UserDetails appuser = null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth != null) {

			Object authObject = auth.getPrincipal();

			if (authObject != null && authObject instanceof String) {
				authObject = auth.getDetails();
			}
			if (authObject != null && authObject instanceof UserDetails) {
				appuser = (UserDetails) authObject;
			}

		}

		return appuser;
	}
	
	public Long getLoggedInUserId() throws AccessDeniedException
	{
		return Optional.ofNullable(userRepository.findByEmail(getLoggedInUser()
				.getUsername()).getId()).orElseThrow(
						() -> new AccessDeniedException("No Logged In User found"));
	}
}
